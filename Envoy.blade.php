@servers(['web' => ['admin@anujsulakhiya.codes']])

@setup
$repository = 'git@gitlab.com:assignment-assist/laravel.git';
$releases_dir = '/web/aa.anujsulakhiya.codes/releases';
$app_dir = '/web/aa.anujsulakhiya.codes';
$release = date('YmdHis');
$new_release_dir = $releases_dir .'/'. $release;
@endsetup

@story('deploy')
clone_repository
@endstory

@story('deploy_final')
clone_repository
run_composer
update_symlinks
@endstory

@task('clone_repository')
cd web/aa.anujsulakhiya.codes/now
git pull origin
php artisan migrate
@endtask

@task('run_composer')
echo "Starting deployment ({{ $release }})"
cd {{ $new_release_dir }}
composer install --prefer-dist --no-scripts -q -o
@endtask


@task('update_symlinks')

echo "Linking storage directory"
rm -rf {{ $new_release_dir }}/storage
ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage

echo 'Linking .env file'
ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env

echo 'Linking current release'
ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current

@endtask

{{--@task('deploy', ['on' => 'aws1'])--}}

{{--cd web/aa.anuj.goclever.in--}}

{{--git pull git@gitlab.com:assignment-assist/laravel.git--}}

{{--composer install--}}


{{--@endtask--}}


{{--@task('install-dependencies')--}}

{{--    cd web/aa.anuj.goclever.in--}}
{{--    composer install--}}

{{--@endtask--}}

{{--@task('run-migration', ['on' => 'aws1'])--}}

{{--cd web--}}
{{--ls--}}

{{--@endtask--}}
