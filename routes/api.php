<?php

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\AutoCompleteController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\CustomerLedgerController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\GlobalController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\Masters\BankMasterController;
use App\Http\Controllers\Masters\CustomerMasterController;
use App\Http\Controllers\Masters\ExpensesMasterController;
use App\Http\Controllers\Masters\FirmMasterController;
use App\Http\Controllers\Masters\MenuMasterController;
use App\Http\Controllers\Masters\StockMasterController;
use App\Http\Controllers\PrintInvoiceController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\StockReturnContoller;
use App\Http\Middleware\isAuthenticated;
use App\Models\Booking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//------- Auth Apis -------//
Route::prefix('auth')->group(function () {
    Route::post('login', [AuthController::class , 'login']);
    Route::get('check_connection', [AuthController::class , 'connectionInfo']);
    Route::patch('changePassword', [AuthController::class , 'changePassword']);

});


Route::middleware([isAuthenticated::class])->group(function () {

    //------- Master Rest Apis -------//
    Route::prefix('master')->group(function () {
        Route::apiResource('firm', FirmMasterController::class);
        Route::apiResource('stock', StockMasterController::class);
        Route::apiResource('customer', CustomerMasterController::class);
        Route::apiResource('bank', BankMasterController::class);
        Route::apiResource('menu', MenuMasterController::class);
        Route::apiResource('expenses', ExpensesMasterController::class);
    });

    //------- Booking, Stock Return, Invoice And Customer Ledger Rest Apis -------//
    Route::apiResource('invoice', InvoiceController::class);
    Route::apiResource('booking', BookingController::class);
    Route::apiResource('stock_return', StockReturnContoller::class);
    Route::apiResource('customer_ledger', CustomerLedgerController::class);

    //------- Dashboard Apis -------//

    //Return data to show summary of booking,invoice,turnover and upcoming bookings
    Route::get('getHomeDetails', [DashboardController::class, 'getHomeDetails']);

    //------- Additional Booking Apis -------//

    //Update customer after invoice is created
    Route::put('booking/updateCustomerOnInvoice/{book_invoice_number}', [BookingController::class, 'updateCustomerOnInvoice']);

    //Return Latest invoice number to create booking
    Route::get('getLatestInvoice', [BookingController::class, 'getLatestInvoice']);

    //Return invoice count of particular firm
    Route::get('getBookingCount/{firm_id}', [BookingController::class, 'getBookingCount']);

    //Delete booking
    Route::delete('booking/{invoice_id}/{is_inventory}',[BookingController::class, 'destroy']);

    //Return all active invoice with due payment
    Route::get('getActiveInvoice/{customer_id}', [InvoiceController::class, 'getActiveInvoice']);

    //------- Additional Stock Return Apis -------//
    Route::get('getStockStatusOnInvoice/{invoice_id}/{service_id}', [StockReturnContoller::class, 'getStockStatusOnInvoice']);
    Route::get('allStockStatusOnInvoice/{invoice_id}', [StockReturnContoller::class, 'allStockStatusOnInvoice']);

    //------- Additional Invoice Apis -------//

    //Return invoices of perticular firm
    Route::get('getInvoiceWithFirm/{invoice_id}/{firm_id}', [PrintInvoiceController::class, 'getInvoiceWithFirm']);

    //this will return all invoices with respect to firms
    Route::get('getInvoiceOfAllFirm/{invoice_id}', [PrintInvoiceController::class, 'getInvoiceOfAllFirm']);

    //Update Payment and Stock status in customer invoices table
    Route::get('setInvoicePaymentAndStock/{invoice_id}', [GlobalController::class, 'setInvoicePaymentAndStock']);

    //Update Invoice Payment
    Route::get('updateInvoicePayment/{invoice_id}', [GlobalController::class, 'updateInvoicePayment']);


    //------- Additional Customer Ledger Apis -------//

    //Generate receipt number for customer ledger
    Route::get('getLatestReceiptNo', [CustomerLedgerController::class, 'getLatestReceiptNo']);

    //------- ledger and report Apis -------//

    //Customer Ledger Report
    Route::get('customerLedgerReport/{customer_id}', [CustomerLedgerController::class, 'customerLedgerReport']);

    //Cashbook Report
    Route::get('getCashbookReport', [ReportController::class, 'getCashbookReport']);

    //Bank Report
    Route::get('getBankbookReport', [ReportController::class, 'getBankbookReport']);

    //------- Autocomplete Apis -------//
    Route::get('autocomplete/{master_keyword}', [AutoCompleteController::class, 'autocomplete']);

    //------- Additional Menu Apis -------//
    Route::get('getMenuCategory', [MenuMasterController::class, 'getMenuCategory']);
});


