<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    public function getCashbookReport(Request $req)
    {
        $where = '';
        $openingWhere = '';
        $openingBalance = 0;
        $details = $req->validate([
            'from_date' => 'nullable',
            'to_date'   => 'nullable',
        ]);

        if(isset($details['from_date']) && isset($details['to_date']))
        {
            $where = "AND tdate BETWEEN '".$details['from_date']."' AND '".$details['to_date']."'";
            $openingWhere = "AND tdate < '".$details['from_date']."'";
            $openingBalance = (
                DB::select(
                    DB::raw(
                        "SELECT SUM(payment_in-payment_out) as opening_balance FROM transactions WHERE payment_id = 1 $openingWhere"
                    )
                )
            )[0]->opening_balance;
        }
        $report = (
            DB::select(
                DB::raw(
                    "SELECT tdate,remark, payment_id , voucher_id , payment_in, payment_out , payment_in-payment_out as balance  , account_id FROM transactions WHERE payment_id = 1 $where ORDER BY tdate"
                )
            )
        );
        return $report = [
            'opening_balance' => $openingBalance,
            'report'          => $report
        ];
    }

    public function getBankbookReport(Request $req)
    {
        $where = '';
        $openingWhere = '';
        $openingBalance = 0;
        $details = $req->validate([
            'from_date' => 'nullable',
            'to_date'   => 'nullable',
        ]);

        if(isset($details['from_date']) && isset($details['to_date']))
        {
            $where = "AND tdate BETWEEN '".$details['from_date']."' AND '".$details['to_date']."'";
            $openingWhere = "AND tdate < '".$details['from_date']."'";

            $openingBalance = (
                DB::select(
                    DB::raw(
                        "SELECT SUM(payment_out-payment_in) as opening_balance FROM transactions WHERE bank_id > 0 $openingWhere"
                    )
                )
            )[0]->opening_balance;
        }

        $report = (
            DB::select(
                DB::raw(
                    "SELECT tdate,remark, payment_id , voucher_id , payment_in, payment_out , payment_out-payment_in as balance  , account_id FROM transactions WHERE bank_id > 0 $where ORDER BY tdate"
                )
            )
        );
        return $report = [
            'opening_balance' => $openingBalance,
            'report'          => $report
        ];
    }


}
