<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\GlobalController;
use App\Rules\MatchOldPassword;
use App\Rules\ShouldNotMatchOldPassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends GlobalController
{
    public function login(Request $req){

        $credentials = $req->validate([
            'email' => 'required|exists:users',
            'password' => 'required|min:6',
        ]);

        $token = Auth::attempt($credentials);

        if ($token ) {
            return $this->respondWithToken($token);
        }

        return response()->json(['error' => 'Unauthorized'], 401);

    }

    public function connectionInfo(){
        return true;
    }

    public function changePassword(Request $req){

        $data = $req->validate([
            'password' =>  ['required', 'min:6', new MatchOldPassword ],
            'newPassword' => ['required', 'min:6', new ShouldNotMatchOldPassword ],
            'confirmNewPassword' => 'required|same:newPassword'
        ]);

        $user = $req->user();
        $newHashedPassword = Hash::make($data['newPassword']);
        $user->password = $newHashedPassword;
        $user->save();
        return $user;
        
    }

     /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            // 'refresh_token' => Auth::refresh(),
            'user' => Auth::user()
        ]);
    }
}
