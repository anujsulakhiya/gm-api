<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AutoCompleteController extends Controller
{

    public $public_master_keyword = [
        'customer' => [
            'tabel_name'            => 'mst_customers',
            'return_column_name'    => 'customer_name',
            'display_column_name'   => 'id,customer_name , customer_address , customer_contact_no, customer_gstin',
            'filter_column'         => 'CONCAT_WS( " ", customer_name , customer_address , customer_contact_no) ',
            'limit'                 => '20',
        ],
        'stock' => [
            'tabel_name'            => '(
                                            SELECT master_stocks.stock_name,
                                                   master_stocks.stock_mrp,
                                                   master_stocks.stock_description,
                                                   master_stocks.stock_available,
                                                   master_firms.firm_name,
                                                   master_stocks.is_inventory,
                                                   master_stocks.id
                                            FROM master_stocks
                                            INNER JOIN master_firms
                                            ON master_stocks.firm_id = master_firms.id
                                        ) as tbl_stocks',
            'return_column_name'    => 'stock_name',
            'display_column_name'   => 'id,stock_name ,stock_description, stock_mrp , stock_available,firm_name,is_inventory',
            'filter_column'         => 'CONCAT_WS(" ", stock_name , firm_name) ',
            'limit'                 => '20',
        ],
    ];

    public function autocomplete(Request $req, $master_keyword)
    {
        $search_keyword  = str_replace(" ", "%", $req->search);
        $data = $this->public_master_keyword[$master_keyword];
        return DB::select(
                "SELECT " . $data['display_column_name'] . " FROM  " . $data['tabel_name'] . " WHERE " . $data['filter_column'] . " LIKE '%" . $search_keyword . "%'  LIMIT " . $data['limit'] . ""
            );
    }
}
