<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\CustomerInvoice;
use App\Models\MstCustomer;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function getHomeDetails()
    {
        $mytime          = Carbon::now();
        $customer        = MstCustomer::count();
        $invoice         = CustomerInvoice::count();
        $turnOver        = CustomerInvoice::sum('invoice_total');
        $upcomingBooking = Booking::where('date' , '>' , $mytime)->count();

        return $data = [
            'customer' => $customer,
            'invoice' => $invoice,
            'turnOver' => $turnOver,
            'upcomingBooking' => $upcomingBooking,
        ];
    }
}
