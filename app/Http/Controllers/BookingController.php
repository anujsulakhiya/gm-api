<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\CustomerInvoice;
use App\Models\CustomerLedger;
use App\Models\MasterStock;
use App\Models\MstCustomer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BookingController extends GlobalController
{

    private $getBookings = [
        'customer_invoices.invoice_number',
        'customer_invoices.invoice_date',
        'customer_invoices.invoice_customer_id',
        'bookings.*',
        'mst_customers.customer_name',
        'mst_customers.customer_address',
        'mst_customers.customer_contact_no',
        'mst_customers.customer_gstin',
        'master_stocks.stock_name',
        'master_stocks.stock_available',
        'master_stocks.is_inventory'
    ];

    public function index(Request $req)
    {
        $users = CustomerInvoice::latest()
            ->join('bookings',      'bookings.invoice_id',  '=', 'customer_invoices.id')
            ->join('mst_customers', 'mst_customers.id',     '=', 'customer_invoices.invoice_customer_id')
            ->join('master_stocks', 'master_stocks.id',     '=', 'bookings.service_id');

        if ($req) {
            $details = $req->validate([
                'invoice_number'        => 'nullable|min:1',
                'bookings.deleted_at'   => 'nullable',
                'bookings.type'         => 'nullable',
            ]);

            $details['bookings.deleted_at'] = null;
            $details['bookings.type'] = 1;

            $users = $users->where($details);
        }
        return  $users->get($this->getBookings);
    }

    public function store(Request $req)
    {
        $details = $req->validate([
            'customer_name'         => 'required',
            'customer_address'      => 'required',
            'customer_contact_no'   => 'required|min:10',
            'customer_gstin'        => 'nullable',
            'invoice_number'        => 'required',
            'invoice_customer_id'   => 'nullable',
            'invoice_date'          => 'nullable',
            'invoice_total'         => 'nullable',
            'type'                  => 'required',
            'invoice_id'            => 'nullable',
            'firm_id'               => 'nullable',
            'firm_invoice_count'    => 'nullable',
            'service_id'            => 'required',
            'rate'                  => 'required',
            'extra_rate'            => 'nullable', 
            'qty_out'               => 'required',
            'date'                  => 'required',
            'days'                  => 'required',
            'time'                  => 'required',
            'total'                 => 'required',
            'is_inventory'          => 'required',
            'stock_available'       => 'required',
            'remaining_stock'       => 'required'
        ]);

        $invoice_details = CustomerInvoice::where('invoice_number', $details['invoice_number'])->get('id');

        if (!$invoice_details->isEmpty()) {
            $details['invoice_id'] = $invoice_details[0]->id;
        } else {

            $customer_details = MstCustomer::where('customer_name', $details['customer_name'])
                ->where('customer_address', $details['customer_address'])
                ->where('customer_contact_no', $details['customer_contact_no'])
                ->get('id');

            if (!$customer_details->isEmpty()) {
                $details['invoice_customer_id'] = $customer_details[0]->id;
            } else {
                $customer_details = MstCustomer::create($req->only(['customer_name', 'customer_address', 'customer_contact_no', 'customer_gstin']));
                $details['invoice_customer_id'] = $customer_details->id;
            }
            $invoice = [
                'invoice_number'      => $details['invoice_number'],
                'invoice_customer_id' => $details['invoice_customer_id'],
                'invoice_date'        => $details['invoice_date'],
                'invoice_total'       => 0,
            ];
            $invoice_id = CustomerInvoice::create($invoice);
            $details['invoice_id'] = $invoice_id->id;
        }

        if ($details['invoice_id']) {

            $details['firm_id'] = (MasterStock::where('id', $details['service_id'])->get('firm_id'))[0]->firm_id;
            $checkInvoiceCount = Booking::where('firm_id', $details['firm_id'])->where('invoice_id', $details['invoice_id'])->get('firm_invoice_count');

            if (!$checkInvoiceCount->isEmpty()) {
                $details['firm_invoice_count'] = $checkInvoiceCount[0]->firm_invoice_count;
            } else {
                $details['firm_invoice_count'] = $this->getBookingCount($details['firm_id']) + 1;
            }

            $firmDetails = Booking::create($details);

            if ($details['is_inventory'] == 0) {
                CustomerInvoice::where('id', $details['invoice_id'])->update(['invoice_total' => DB::raw('invoice_total + ' . $details['total'] . '')]);
            }

            if ($firmDetails->id && $details['is_inventory'] == 1) {
                MasterStock::where('id', $details['service_id'])->update(['stock_available' => DB::raw('stock_available - ' . $details['qty_out'] . '')]);
            }

            $this->setInvoicePaymentAndStock($details['invoice_id']);
        }
        return $this->index($req);
    }

    public function update(Request $req, $id)
    {
        $details = $req->validate([
            'service_id' => 'required',
            'rate'       => 'required',
            'extra_rate' => 'required',
            'qty_out'    => 'required',
            'date'       => 'required',
            'days'       => 'required',
            'time'       => 'required',
            'total'      => 'required',
        ]);

        $booking = (Booking::where('id', $id)->get())[0];
        $qty_now = (MasterStock::where('id', $details['service_id'])->get('stock_available'))[0]->stock_available;

        $updated_qty = $qty_now - ($details['qty_out'] - $booking->qty_out);

        MasterStock::where('id', $details['service_id'])->update(['stock_available' => $updated_qty]);
        Booking::find($id)->update($details);

        $this->setInvoicePaymentAndStock($id);
        return Booking::find($id);
    }

    public function updateCustomerOnInvoice(Request $req, $book_invoice_number)
    {
        $details = $req->validate([
            'customer_name'      =>  'required',
            'customer_address'   => 'required',
            'customer_contact_no' => 'required|min:10',
            'customer_gstin'     => 'nullable',
        ]);

        $customer_details = MstCustomer::where('customer_name', $details['customer_name'])->get('id');

        if (!$customer_details->isEmpty()) {
            $book_customer_id = $customer_details[0]->id;
        } else {
            $customer_details = MstCustomer::create($req->only(['customer_name', 'customer_address', 'customer_contact_no', 'customer_gstin']));
            $book_customer_id = $customer_details->id;
        }

        $invoice_id = (CustomerInvoice::where('invoice_number', $book_invoice_number)->get('id'))[0]->id;

        CustomerInvoice::where('id', $invoice_id)->update(['invoice_customer_id' => $book_customer_id]);
        return Booking::where('id', $invoice_id)->get();
    }

    public function getLatestInvoice()
    {
        return CustomerInvoice::latest()->first();
    }

    public function getBookingCount($firm_id)
    {
        return Booking::select('invoice_id')->where('firm_id', $firm_id)->groupBy('invoice_id')->get()->count();
    }

    public function show($id)
    {
        return Booking::join('master_stocks', 'master_stocks.id', '=', 'bookings.service_id')->find($id);
    }

    public function destroy($invoice_id, $is_inventory)
    {
        $booking = (Booking::where('id', $invoice_id)->get())[0];
        Booking::find($invoice_id)->delete();
        $this->setInvoicePaymentAndStock($invoice_id);

        if ($is_inventory == 1) {
            MasterStock::where('id', $booking->service_id)->update(['stock_available' => DB::raw('stock_available + ' . $booking->qty_out . '')]);
        }
        return response()->json('', 204);
    }
}
