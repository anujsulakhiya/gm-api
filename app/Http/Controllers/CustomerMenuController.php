<?php

namespace App\Http\Controllers;

use App\Models\CustomerMenu;
use Illuminate\Http\Request;

class CustomerMenuController extends Controller
{
    private $getCustomerMenu = [
        'id',
        'customer_id',
        'menu_category',
        'menu_name',
        'schedule',
        'created_at'
    ];

    public function index(Request $req)
    {
        return CustomerMenu::latest()->get($this->getCustomerMenu);
    }

    public function store(Request $req)
    {
        $details = $req->validate([
            'customer_id'   => 'required',
            'menu_date'     => 'required',
            'remark'        => 'required',
            'menu_category' => 'required',
            'menu_name'     => 'required',
            'schedule'      => 'required',
        ]);

        $menuDetails = CustomerMenu::create($details);
        return $menuDetails;
    }

    public function update(Request $req,$id)
    {
        $details = $req->validate([
            'customer_id'   => 'required',
            'menu_date'     => 'required',
            'remark'        => 'required',
            'menu_category' => 'required',
            'menu_name'     => 'required',
            'schedule'      => 'required',
        ]);

        CustomerMenu::find($id)->update($details);
        return CustomerMenu::find($id);
    }

    public function show($id)
    {
        return CustomerMenu::find($id);
    }

    public function destroy($id)
    {
        CustomerMenu::find($id)->delete();
        return response()->json('', 204);
    }
}
