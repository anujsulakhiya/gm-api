<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\CustomerInvoice;
use App\Models\MasterStock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class StockReturnContoller extends GlobalController
{
    //--------------- Stock Return ---------------//
    private $getBookings = [
        'customer_invoices.invoice_number',
        'customer_invoices.invoice_date',
        'customer_invoices.invoice_customer_id',
        'bookings.*',
        'mst_customers.customer_name',
        'mst_customers.customer_address',
        'mst_customers.customer_contact_no',
        'mst_customers.customer_gstin',
        'master_stocks.stock_name',
        'master_stocks.stock_available',
        'master_stocks.stock_value'
    ];

    public function index(Request $req)
    {
        $book = CustomerInvoice::latest()
        ->join('bookings',      'bookings.invoice_id',  '=', 'customer_invoices.id')
        ->join('mst_customers', 'mst_customers.id',     '=', 'customer_invoices.invoice_customer_id')
        ->join('master_stocks', 'master_stocks.id',     '=', 'bookings.service_id')
        ->where('master_stocks.is_inventory', 1);

        if($req)
        {
            $details = $req->validate([
                'invoice_number'      => 'nullable|min:1',
                'bookings.deleted_at' => 'nullable',
                'type'                => 'nullable',
            ]);
            $details['bookings.deleted_at'] = null;
            $book->where($details);
        }
        $booking = $book->get($this->getBookings);
        foreach($booking as $i => $book)
        {
            $stock = $this->getStockStatusOnInvoice($book->invoice_id, $book->service_id );
            $book->qty_remaining = $stock['qty_remaining'];
        }
        return $booking;
    }

    public function getStockStatusOnInvoice($invoice_id,$service_id)
    {
        $rate = (
            Booking::join('master_stocks','master_stocks.id', '=', 'bookings.service_id')
            ->where('bookings.invoice_id' , $invoice_id)
            ->where('bookings.service_id' , $service_id)
            ->where('bookings.type' , 1)
            ->where('master_stocks.is_inventory', 1)
            ->get(['bookings.rate','bookings.extra_rate'])
        );

        $details['qty_book'] = (
            Booking::join('master_stocks','master_stocks.id', '=', 'bookings.service_id')
            ->where('bookings.invoice_id' , $invoice_id)
            ->where('bookings.service_id' , $service_id)
            ->where('bookings.type' , 1)
            ->where('master_stocks.is_inventory', 1)
            ->get('bookings.qty_out')[0]->qty_out
        );

        $details['qty_return'] = (
            Booking::where('invoice_id' , $invoice_id)
            ->where('service_id' , $service_id)
            ->where('type' , 2)
            ->sum('qty_in')
    );

        $details['qty_lost'] = (
            Booking::where('invoice_id' , $invoice_id)
            ->where('service_id' , $service_id)
            ->where('type' , 2)
            ->sum('qty_lost')
        );

        if(!$details['qty_book'] == null){

            $details['invoice_id']    = $invoice_id;
            $details['service_id']    = $service_id;
            $details['rate']          = $rate[0]->rate + $rate[0]->extra_rate;
            $details['qty_remaining'] = $details['qty_book'] - ( $details['qty_return'] + $details['qty_lost'] );
            return $details;
        }
        return 'Service Not Found';
    }

    public function allStockStatusOnInvoice($invoice_id)
    {
        $services = (
            Booking::join('master_stocks','master_stocks.id', '=', 'bookings.service_id')
            ->where('bookings.invoice_id' , $invoice_id)
            ->where('master_stocks.is_inventory', 1)
            ->groupBy('bookings.service_id')
            ->get('bookings.service_id')
        );

        if(!$services->isEmpty())
        {
            foreach($services as $i => $service)
            {
                $stock = $this->getStockStatusOnInvoice($invoice_id,$service->service_id);
                $data[$i] = [
                    'service_id' => $service->service_id,
                    'qty_remaining' => $stock['qty_remaining']
                ];
            }
            return $data;
        }
        return 'No service found';
    }

    public function store(Request $req)
    {
        $details = $req->validate([
            'type'              => 'nullable',
            'is_inventory'      => 'required',
            'invoice_id'        => 'nullable',
            'firm_id'           => 'nullable',
            'firm_invoice_count'=> 'nullable',
            'service_id'        => 'required',
            'rate'              => 'required',
            'qty_in'            => 'required',
            'qty_lost'          => 'required',
            'lost_value'        => 'required',
            'date'              => 'required',
            'days'              => 'required',
            'time'              => 'nullable',
            'total'             => 'required',
        ]);

        $details['firm_id'] = (MasterStock::where('id', $details['service_id'])->get('firm_id'))[0]->firm_id;

        $details['firm_invoice_count'] = (
            Booking::where('service_id', $details['service_id'])
            ->where('invoice_id', $details['invoice_id'])
            ->where('type', 1)
            ->get('firm_invoice_count')
        )[0]->firm_invoice_count;

        $stockReturn = Booking::create($details);

        if($stockReturn->id)
        {
            $this->setInvoicePaymentAndStock($details['invoice_id']);
            MasterStock::where('id',$details['service_id'])->update(['stock_available' => DB::raw('stock_available + '.$details['qty_in'].'')]);
        }
        return $this->index($req);
    }

    public function getBookingCount($firm_id){
        return Booking::select('invoice_id')->where('firm_id', $firm_id)->groupBy('invoice_id')->get()->count();
    }

    public function update(Request $req,$id)
    {
        $details = $req->validate([
            'service_id' => 'required',
            'rate'       => 'required',
            'qty_in'     => 'required',
            'qty_lost'   => 'required',
            'lost_value' => 'required',
            'date'       => 'required',
            'days'       => 'required',
            'time'       => 'required',
            'total'      => 'required',
        ]);

        $booking = (Booking::where('id',$id)->get())[0];
        $qty_now = (MasterStock::where('id',$details['service_id'])->get('stock_available'))[0]->stock_available;
        $updated_qty = $qty_now + (  $details['qty_in'] - $booking->qty_in );

        MasterStock::where('id', $details['service_id'])->update(['stock_available' => $updated_qty]);
        Booking::find($id)->update($details);

        return Booking::find($id);
    }

    public function show($id)
    {

    }

    public function destroy($id)
    {
        $booking = (Booking::where('id',$id)->get())[0];
        $qty_now = (MasterStock::where('id',$booking->service_id)->get('stock_available'))[0]->stock_available;
        $updated_qty = $qty_now - $booking->qty_in;

        MasterStock::where('id', $booking->service_id)->update(['stock_available' => $updated_qty]);
        Booking::find($id)->delete();

        $this->setInvoicePaymentAndStock($booking->invoice_id);
        return response()->json('', 204);
    }
}
