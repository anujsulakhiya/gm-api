<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\CustomerInvoice;
use App\Models\MasterFirm;
use App\Models\MstCustomer;
use Illuminate\Support\Facades\DB;


class PrintInvoiceController extends Controller
{

    public function getInvoiceOfAllFirm($invoice_id)
    {
        $firm_id =  Booking::where('invoice_id',$invoice_id)->where('type',1)->groupBy('firm_id')->get('firm_id');
        $countFirm = count($firm_id);

        for ($i=0; $i < $countFirm; $i++)
        {
            $invoiceData = ($this->getInvoiceWithFirm($firm_id[$i]['firm_id'],$invoice_id));
            $data[$i]['firm_details'] = $invoiceData['firm_details'];
            $data[$i]['invoice_number'] = $invoiceData['invoice_number'];
            $data[$i]['invoice'] = $invoiceData['invoice'];
            $data[$i]['invoiceTotal'] = $invoiceData['invoiceTotal'];
            $data[$i]['customer'] = $invoiceData['customer'];
            $data[$i]['bookingDetails'] = $invoiceData['bookingDetails'];
        }
        return $data;
    }

    public function getInvoiceWithFirm($firm_id,$invoice_id)
    {
        $bookingDetails =  (
            DB::select(
                "Select * from bookings Where ((is_inventory  = 1 AND TYPE = 2  ) OR is_inventory = 0) AND invoice_id = '$invoice_id' AND firm_id = '$firm_id' AND deleted_at is null"
            )
        );

        $invoiceTotal =  (
            DB::select(
                "Select SUM((rate*(qty_in+qty_out)*days)+(lost_value*qty_lost) ) as invoiceTotal from bookings Where ((is_inventory  = 1 AND TYPE = 2  ) OR is_inventory = 0) AND invoice_id = '$invoice_id' AND firm_id = '$firm_id' AND deleted_at is null"
            )
        );

        if(!isset($invoiceTotal[0]))
        {
            $invoiceTotal  = 0;
        }
        else
        {
            $invoiceTotal = $invoiceTotal[0]->invoiceTotal;
        }

        if(!isset($bookingDetails[0]))
        {
            $invoice_number  = 0;
        }
        else
        {
            $invoice_number =  $bookingDetails[0]->firm_invoice_count;
        }

        $invoice = (CustomerInvoice::where('id',$invoice_id)->get())[0];
        $customer = (MstCustomer::where('id',$invoice->invoice_customer_id)->get())[0];

        return $data = [
            'firm_details' => (MasterFirm::where('id',$firm_id)->get())[0],
            'invoice_number' => $invoice_number,
            'invoice' => $invoice,
            'invoiceTotal' => $invoiceTotal,
            'customer' => $customer,
            'bookingDetails' => $bookingDetails
        ];
    }
}
