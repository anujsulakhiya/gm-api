<?php

namespace App\Http\Controllers;

use App\Models\CustomerInvoice;
use App\Models\CustomerLedger;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CustomerLedgerController extends GlobalController
{

    private $getledger = [
    'id',
    'account_id',
    'voucher_id',
    'voucher_head',
    'tdate',
    'customer_id',
    'invoice_id',
    'expense_id',
    'employee_id',
    'debit',
    'payment_in',
    'payment_out',
    'discount',
    'remark',
    'payment_id',
    'bank_id',
    'cheque_no',
    'cheque_status',
    'cheque_date',
    'cheque_clear_date',
    'created_at'];

    public function index(Request $req)
    {
        $ledger = Transaction::latest();

        if($req)
        {
            $details = $req->validate([
                'customer_id' => 'required',
                'from_date' => 'nullable',
                'to_date' => 'nullable',
            ]);
            $ledger = $ledger->where('customer_id',$details['customer_id']);

            if(isset($details['from_date']) && isset($details['to_date']))
            {
                $ledger->whereBetween('tdate', [$details['from_date'], $details['to_date']]);
            }
        }
        $data = [
            'ActiveInvoice' => $this->getActiveInvoice($details['customer_id']),
            'ledger' => $this->customerLedgerReport($req,$details['customer_id']),
        ];
        return \response()->json($data);
    }

    public function customerLedgerReport(Request $req,$customer_id)
    {
        $where = '';
        $openingWhere = '';
        $openingBalance = 0;

        $details = $req->validate([
            'from_date' => 'nullable',
            'to_date'   => 'nullable',
        ]);

        if(isset($details['from_date']) && isset($details['to_date']))
        {
            $where  = "WHERE tdate BETWEEN '".$details['from_date']."' AND '".$details['to_date']."' ";
            $openingWhere = "WHERE tdate < '".$details['from_date']."' ";

            $openingBalance = (DB::select(
                DB::raw(
                    "SELECT SUM(balance) as opening_balance FROM (
                        SELECT  invoice_date as tdate, invoice_total as balance FROM customer_invoices WHERE invoice_customer_id = $customer_id AND deleted_at IS NULL

                        UNION ALL

                        SELECT tdate, payment_out-payment_in + debit-discount as balance FROM transactions WHERE customer_id = $customer_id AND deleted_at IS NULL
                    ) as customer_ledger $openingWhere"
                )
            ))[0]->opening_balance;
        }
        $report = DB::select(
            DB::raw(
                "SELECT * FROM (
                    SELECT id,
                    invoice_date as tdate,
                    'Booking' as remark ,
                    invoice_number ,
                    0 as payment_mode,
                    invoice_total as bill_amount ,
                    0 as paid_amount,
                    0 as extra_amount,
                    0 as discount ,
                    invoice_total as balance FROM customer_invoices WHERE invoice_customer_id = $customer_id AND deleted_at IS NULL

                    UNION ALL

                    SELECT id,
                    tdate ,
                    remark,
                    voucher_id ,
                    payment_id ,
                    0 as bill_amount,
                    payment_in + (-1 * payment_out)  ,
                    debit ,
                    discount,
                     payment_out-payment_in + debit-discount as balance FROM transactions WHERE customer_id = $customer_id AND deleted_at IS NULL
                ) as customer_ledger $where ORDER BY tdate"
            )
        );

        return $report = [
            'opening_balance' => $openingBalance,
            'report'          => $report
        ];
    }

    public function getLatestReceiptNo()
    {
        $count = Transaction::where('account_id', 1)->count();
        return $count+1;
    }


    public function store(Request $req)
    {
        $details = $req->validate([
            'tdate'            => 'required',
            'voucher_id'       => 'required',
            'account_id'       => 'required',
            'customer_id'      => 'required',
            'invoice_id'       => 'required',
            'debit'            => 'required',
            'payment_in'       => 'required',
            'payment_out'      => 'required',
            'discount'         => 'nullable',
            'payment_id'       => 'required',
            'bank_id'          => 'nullable',
            'cheque_no'        => 'nullable',
            'cheque_clear_date'=> 'nullable',
            'cheque_date'      => 'nullable',
            'remark'           => 'nullable',
        ]);

        Transaction::create($details);
        $this->updateInvoicePayment($details['invoice_id']);
        return $this->index($req);
    }

    public function update(Request $req,$id)
    {
        $details = $req->validate([
            'tdate'             => 'required',
            'invoice_id'        => 'required',
            'debit'             => 'required',
            'payment_in'        => 'required',
            'payment_out'       => 'required',
            'discount'          => 'nullable',
            'payment_id'        => 'required',
            'bank_id'           => 'nullable',
            'cheque_no'         => 'nullable',
            'cheque_clear_date' => 'nullable',
            'cheque_date'       => 'nullable',
            'remark'            => 'nullable',
        ]);

        Transaction::find($id)->update($details);
        $this->updateInvoicePayment($details['invoice_id']);
        return Transaction::find($id);
    }

    public function show($id)
    {
        return Transaction::find($id);
    }

    public function destroy($id)
    {
        $invoice_id = Transaction::find($id)->invoice_id;
        Transaction::find($id)->delete();
        $this->updateInvoicePayment($invoice_id);
        return response()->json('', 204);
    }
}
