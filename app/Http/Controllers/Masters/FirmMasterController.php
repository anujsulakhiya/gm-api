<?php

namespace App\Http\Controllers\Masters;

use App\Http\Controllers\GlobalController;
use App\Models\MasterFirm;
use Illuminate\Http\Request;

class FirmMasterController extends GlobalController
{
     //--------------- Firm Master ---------------//

    private $getFirm = ['id', 'firm_name', 'firm_contact_no', 'firm_address', 'firm_gstin','created_at'];


    public function index(Request $req){

        return MasterFirm::latest()->get($this->getFirm);

    }

    public function store(Request $req){

        $details = $req->validate([
            'firm_name' => 'required|unique:master_firms',
            'firm_contact_no'  =>  'required|min:10',
            'firm_address' => 'required',
            'firm_gstin' => 'nullable',
        ]);

        $firmDetails = MasterFirm::create($details);
        return $firmDetails;

    }

    public function update(Request $req,$id){

        $details = $req->validate([
            'firm_name' => 'required|unique:master_firms,firm_name,'.$id,
            'firm_contact_no'  =>  'required|min:10',
            'firm_address' => 'required',
            'firm_gstin' => 'nullable',
        ]);

        MasterFirm::find($id)->update($details);
        return MasterFirm::find($id);
    }

    public function show($id){
        return MasterFirm::find($id);
    }

    public function destroy($id){

        MasterFirm::find($id)->delete();
        return response()->json('', 204);
    }
}
