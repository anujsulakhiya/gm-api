<?php

namespace App\Http\Controllers\Masters;

use App\Http\Controllers\Controller;
use App\Models\MstMenu;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class MenuMasterController extends Controller
{
    private $getMenu = ['id', 'menu_category', 'menu_name','created_at'];


    public function index(Request $req)
    {
        $menu = MstMenu::latest();
        if ($req)
        {
            $details = $req->validate([
                'menu_category' => 'nullable',
            ]);

            $menu = $menu->where($details);
        }
        return  $menu->get($this->getMenu);
    }

    public function getMenuCategory(){
        return MstMenu::groupBy('menu_category')->get('menu_category');
    }

    public function store(Request $req)
    {
        $variable = $req->menu_category;
        $details = $req->validate([
            'menu_category' => 'required' ,
            'menu_name' => Rule::unique('mst_menus', 'menu_name')->where(function ($query) use ($variable) {
                $query->where('menu_category', $variable);
            }),
        ]);

        $menuDetails = MstMenu::create($details);
        return $menuDetails;

    }

    public function update(Request $req,$id){
        $variable = $req->menu_category;
        
        $details = $req->validate([
            'menu_category' => 'required',
            'menu_name' => Rule::unique('mst_menus', 'menu_name')->where(function ($query) use ($variable) {
                $query->where('menu_category', $variable);
            }),
        ]);

        MstMenu::find($id)->update($details);
        return MstMenu::find($id);
    }

    public function show($id){
        return MstMenu::find($id);
    }

    public function destroy($id){

        MstMenu::find($id)->delete();
        return response()->json('', 204);
    }
}
