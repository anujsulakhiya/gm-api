<?php

namespace App\Http\Controllers\Masters;

use App\Http\Controllers\Controller;
use App\Models\MstExpenses;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ExpensesMasterController extends Controller
{
    private $getExpense = ['id', 'head', 'sub_head', 'created_at'];

    public function index(Request $req)
    {
        return MstExpenses::latest()->get($this->getExpense);
    }

    public function store(Request $req)
    {
        $head = $req->head;
        $details = $req->validate([
            'head'      => 'required',
            'sub_head'  => Rule::unique('mst_expenses', 'sub_head')->where(function ($query) use ($head) {
                            $query->where('head', $head);
                        }),
        ]);

        $expensesHead = MstExpenses::create($details);
        return $expensesHead;
    }

    public function update(Request $req,$id)
    {
        $head = $req->head;
        $details = $req->validate([
            'head'      => 'required',
            'sub_head'  => Rule::unique('mst_expenses', 'sub_head')->where(function ($query) use ($head) {
                            $query->where('head', $head);
                        }),
        ]);

        MstExpenses::find($id)->update($details);
        return MstExpenses::find($id);
    }

    public function show($id)
    {
        return MstExpenses::find($id);
    }

    public function destroy($id)
    {
        MstExpenses::find($id)->delete();
        return response()->json('', 204);
    }
}
