<?php

namespace App\Http\Controllers\Masters;

use App\Http\Controllers\Controller;
use App\Models\MstBank;
use Illuminate\Http\Request;

class BankMasterController extends Controller
{
    private $getFirm = ['id', 'bank_name', 'account_no', 'ifsc_code', 'branch','created_at'];


    public function index(Request $req){

        return MstBank::latest()->get($this->getFirm);

    }

    public function store(Request $req){

        $details = $req->validate([
            'bank_name' => 'required',
            'account_no'  =>  'required|unique:mst_banks',
            'ifsc_code' => 'required',
            'branch' => 'nullable',
        ]);

        $firmDetails = MstBank::create($details);
        return $firmDetails;

    }

    public function update(Request $req,$id){

        $details = $req->validate([

            'bank_name' => 'required',
            'account_no'  =>  'required|unique:mst_banks,account_no,'.$id,
            'ifsc_code' => 'required',
            'branch' => 'nullable',
        ]);

        MstBank::find($id)->update($details);
        return MstBank::find($id);
    }

    public function show($id){
        return MstBank::find($id);
    }

    public function destroy($id){

        MstBank::find($id)->delete();
        return response()->json('', 204);
    }
}
