<?php

namespace App\Http\Controllers\Masters;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\MasterStock;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class StockMasterController extends Controller
{
    //--------------- Firm Master ---------------//

    private $getStock = ['id', 'firm_id', 'stock_name',  'stock_description','stock_mrp', 'stock_qty','stock_available','stock_gst','stock_type', 'is_inventory','stock_value','created_at'];


    public function index(Request $req){

        return MasterStock::latest()->get($this->getStock);
    }

    public function getAvailableStock(Request $req){

        $details = $req->validate([
            'stock_id' => 'required',
        ]);

        $stock_out = (Booking::where('service_id', $details['stock_id'])->get('qty_in'))[0]->qty_in;

        $stcok_in = '';
    }

    public function store(Request $req){

        $variable = $req->firm_id;
        $details = $req->validate([
            'firm_id' => 'required',
            // 'stock_name'  =>  'required|unique:master_stocks',
            'stock_name' => Rule::unique('master_stocks', 'stock_name')->where(function ($query) use ($variable) {
                $query->where('firm_id', $variable);
            }),
            'stock_description'  =>  'nullable',
            'stock_mrp' => 'required',
            'stock_qty' => 'nullable',
            'stock_available' => 'nullable',
            'stock_gst' => 'nullable',
            'stock_type' => 'required',
            'is_inventory' => 'required',
            'stock_value' => 'nullable',
        ]);

        $details['stock_available'] = $details['stock_qty'];
        $stockDetails = MasterStock::create($details);
        return $stockDetails;

    }

    public function update(Request $req,$id){

        $details = $req->validate([
            'firm_id' => 'required|',
            'stock_name' => 'required|unique:master_stocks,stock_name,'.$id,
            'stock_description'  =>  'nullable',
            'stock_mrp' => 'required',
            'stock_qty' => 'nullable',
            'stock_available' => 'nullable',
            'stock_gst' => 'nullable',
            'stock_type' => 'required',
            'is_inventory' => 'required',
            'stock_value' => 'nullable',
        ]);

        MasterStock::find($id)->update($details);
        return MasterStock::find($id);
    }

    public function show($id){
        return MasterStock::find($id);
    }

    public function destroy($id){

        MasterStock::find($id)->delete();
        return response()->json('', 204);
    }
}
