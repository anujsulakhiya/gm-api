<?php

namespace App\Http\Controllers\Masters;

use App\Http\Controllers\Controller;
use App\Models\MstCustomer;
use Illuminate\Http\Request;

class CustomerMasterController extends Controller
{
    //--------------- Customer Master ---------------//

    private $getFirm = ['id', 'customer_name', 'customer_address', 'customer_contact_no', 'customer_gstin','created_at'];


    public function index(Request $req){

        return MstCustomer::latest()->get($this->getFirm);

    }

    public function store(Request $req){

        $details = $req->validate([
            'customer_name' => 'required|unique:mst_customers',
            'customer_address'  =>  'required',
            'customer_contact_no' =>  'nullable|min:10',
            'customer_gstin' => 'nullable',
        ]);

        $firmDetails = MstCustomer::create($details);
        return $firmDetails;

    }

    public function update(Request $req,$id){

        $details = $req->validate([
            'customer_name' => 'required|unique:mst_customers,customer_name,'.$id,
            'customer_address'  =>  'required',
            'customer_contact_no' =>  'nullable|min:10',
            'customer_gstin' => 'nullable',
        ]);

        MstCustomer::find($id)->update($details);
        return MstCustomer::find($id);
    }

    public function show($id){
        return MstCustomer::find($id);
    }

    public function destroy($id){

        MstCustomer::find($id)->delete();
        return response()->json('', 204);
    }
}
