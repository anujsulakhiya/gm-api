<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\CustomerInvoice;
use App\Models\CustomerLedger;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InvoiceController extends GlobalController
{
    //--------------- invoice Master ---------------//

    private $getinvoice = [
        'customer_invoices.id',
        'customer_invoices.invoice_number',
        'customer_invoices.invoice_customer_id',
        'customer_invoices.invoice_date',
        'customer_invoices.invoice_total',
        'customer_invoices.created_at',
        'mst_customers.customer_name',
        'mst_customers.customer_contact_no'
    ];


    public function index(Request $req)
    {
        $invoice = CustomerInvoice::latest()->join('mst_customers', 'mst_customers.id', '=', 'customer_invoices.invoice_customer_id');
        $filter = $req->validate([
            'from_date' => 'nullable',
            'to_date'   => 'nullable',
        ]);

        if(isset($filter['from_date']) && isset($filter['to_date']))
        {
            $invoice->whereBetween('customer_invoices.invoice_date', [$filter['from_date'], $filter['to_date']]);
        }
        return  $invoice->get($this->getinvoice);
    }

    public function store(Request $req)
    {
        $details = $req->validate([
            'invoice_number'      => 'required',
            'invoice_customer_id' => 'nullable',
            'invoice_date'        => 'nullable',
            'invoice_total'       => 'nullable',
        ]);

        $invoiceDetails = CustomerInvoice::create($details);
        return $invoiceDetails->id;
    }

    public function update(Request $req,$id)
    {
        $details = $req->validate([
            'invoice_name'        => 'required|unique:customer_invoices,invoice_name,'.$id,
            'invoice_customer_id' => 'nullable',
            'invoice_date'        => 'nullable',
            'invoice_total'       => 'nullable',
        ]);

        CustomerInvoice::find($id)->update($details);
        return CustomerInvoice::find($id);
    }

    public function show($id)
    {
        return CustomerInvoice::find($id);
    }

    public function destroy($id)
    {
        CustomerInvoice::find($id)->forceDelete();
        return response()->json('', 204);
    }

    public function showInvoice($req)
    {
        $this->show($req);
    }
}
