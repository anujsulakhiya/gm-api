<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\CustomerInvoice;
use Illuminate\Support\Facades\DB;

class GlobalController extends Controller
{
    public function getActiveInvoice($customer_id)
    {
        return DB::select(
            'SELECT * FROM `customer_invoices` WHERE invoice_customer_id = '.$customer_id.' AND (total_remaining_qty > 0 OR ( (invoice_total + debit) - (paid_amount + discount)  > 0 OR (invoice_total + debit) - (paid_amount + discount)  < 0)) AND deleted_at is NULL'
        );
    }

    public function updateInvoicePayment($invoice_id)
    {
        $transaction =  (
            DB::select(
                'SELECT sum(payment_in) as payment_in, sum(payment_out) as payment_out, sum(discount) as discount, sum(debit) as debit FROM `transactions` WHERE invoice_id = '.$invoice_id.' AND account_id = 1 AND deleted_at is null'
            )
        );
        $details['paid_amount'] = (
            ($transaction[0]->payment_in - $transaction[0]->payment_out) ? $transaction[0]->payment_in - $transaction[0]->payment_out : 0
        );
        $details['discount'] = (
            ($transaction[0]->discount) ? $transaction[0]->discount : 0
        );
        $details['debit'] = (
            ($transaction[0]->debit) ? $transaction[0]->debit : 0
        );
        CustomerInvoice::where('id',$invoice_id)->update([
            'paid_amount' => $details['paid_amount'],
            'discount'    => $details['discount'],
            'debit'       => $details['debit'],
        ]);
        return $details;
    }

    public function setInvoicePaymentAndStock($invoice_id)
    {
        $details['total_remaining_qty'] = (
            Booking::join('master_stocks','master_stocks.id', '=', 'bookings.service_id')
            ->where('invoice_id', $invoice_id)
            ->where('master_stocks.is_inventory', 1)
            ->sum(DB::raw('qty_out - (qty_in + qty_lost)'))
        );

        $stock_return_total = (
            Booking::join('master_stocks','master_stocks.id', '=', 'bookings.service_id')
            ->where('invoice_id', $invoice_id)
            ->where('master_stocks.is_inventory', 1)
            ->where('type', 2)->sum('total')
        );

        $non_inventory_total = (
            Booking::join('master_stocks','master_stocks.id', '=', 'bookings.service_id')
            ->where('invoice_id', $invoice_id)
            ->where('master_stocks.is_inventory', 0)
            ->where('type', 1)->sum('total')
        );

        $details['invoice_total'] = $stock_return_total + $non_inventory_total;
        CustomerInvoice::where('id',$invoice_id)->update($details);
        return $details;
    }
}
