<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerLedger extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */

    protected $fillable = [
        'customer_id',
        'invoice_id',
        'payment_type',
        'bill_amount',
        'paid_amount',
        'discount',
        'amount',
        'remark',
        'tdate'
    ];
}
