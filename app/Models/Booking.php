<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'type',
        'is_inventory',
        'invoice_id',
        'service_id',
        'firm_id',
        'firm_invoice_count',
        'rate',
        'extra_rate',
        'qty_in',
        'qty_out',
        'qty_lost',
        'lost_value',
        'date',
        'days',
        'time',
        'total',
    ];
}
