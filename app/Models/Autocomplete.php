<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Autocomplete extends Model
{
    use HasFactory;

     /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'master_keyword',
        'table_name',
        'return_column_name',
        'filter_column',
        'search_keyword',
        'limit',
    ];
}
