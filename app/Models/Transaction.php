<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */


    protected $fillable = [
        'account_id',
        'voucher_id',
        'voucher_head',
        'tdate',
        'customer_id',
        'invoice_id',
        'expense_id',
        'employee_id',
        'debit',
        'payment_in',
        'payment_out',
        'discount',
        'remark',
        'payment_id',
        'bank_id',
        'cheque_no',
        'cheque_status',
        'cheque_date',
        'cheque_clear_date',
    ];
}
