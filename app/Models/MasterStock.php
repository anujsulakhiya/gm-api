<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterStock extends Model
{
    use HasFactory;


     /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'firm_id',
        'stock_name',
        'stock_description',
        'stock_mrp',
        'stock_qty',
        'stock_available',
        'stock_gst',
        'stock_type',
        'is_inventory',
        'stock_value',
    ];

}
