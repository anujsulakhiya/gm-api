<?php

namespace Database\Seeders;

use App\Models\MstBank;
use Illuminate\Database\Seeder;

class BankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MstBank::factory()->count(5)->create();
    }
}
