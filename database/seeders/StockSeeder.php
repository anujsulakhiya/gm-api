<?php

namespace Database\Seeders;

use App\Models\MasterStock;
use Illuminate\Database\Seeder;

class StockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterStock::factory()->count(100)->create();
    }
}
