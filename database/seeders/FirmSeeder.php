<?php

namespace Database\Seeders;

use App\Models\MasterFirm;
use Illuminate\Database\Seeder;

class FirmSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterFirm::factory()->count(10)->create();
    }
}
