<?php

namespace Database\Seeders;

use App\Models\MasterFirm;
use App\Models\MasterStock;
use App\Models\MstCustomer;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Shantivatika Garden',
            'email' => 'admin',
            'password' => Hash::make('123456'),
        ]);


    //     $details = [
    //         [
    //             'firm_name' => 'santikunj',
    //             'firm_contact_no' => 1234567890,
    //             'firm_address' => 'betma',
    //             'firm_gstin' => 123,
    //         ],
    //         [
    //             'firm_name' => 'santikunj',
    //             'firm_contact_no' => 1234567890,
    //             'firm_address' => 'betma',
    //             'firm_gstin' => 123,
    //         ]
    //     ];

    //     MasterFirm::insert($details);

    //     $details = [
    //         [
    //             'firm_id' => 1,
    //             'stock_name' => 'Garden',
    //             'stock_mrp' => 10000,
    //             'stock_qty' => 1,
    //             'stock_available' => 1,
    //             'stock_gst' => 0,
    //             'stock_type' => 0,
    //             'is_inventory' => 0,
    //             'stock_value' => 0,
    //         ],
    //         [
    //             'firm_id' => 1,
    //             'stock_name' => 'Table',
    //             'stock_mrp' => 15,
    //             'stock_qty' => 100,
    //             'stock_available' => 100,
    //             'stock_gst' => 0,
    //             'stock_type' => 0,
    //             'is_inventory' => 1,
    //             'stock_value' => 250,
    //         ],
    //         [
    //             'firm_id' => 1,
    //             'stock_name' => 'Chair',
    //             'stock_mrp' => 10,
    //             'stock_qty' => 200,
    //             'stock_available' => 200,
    //             'stock_gst' => 0,
    //             'stock_type' => 0,
    //             'is_inventory' => 1,
    //             'stock_value' => 200,
    //         ],
    //         [
    //             'firm_id' => 1,
    //             'stock_name' => 'Mate',
    //             'stock_mrp' => 25,
    //             'stock_qty' => 50,
    //             'stock_available' => 50,
    //             'stock_gst' => 0,
    //             'stock_type' => 0,
    //             'is_inventory' => 1,
    //             'stock_value' => 350,
    //         ]
    //     ];

    //     MasterStock::insert($details);


    //     $details = [
    //         [
    //             'customer_name' => 'Anuj',
    //             'customer_contact_no' => 1234567890,
    //             'customer_address' => 'betma',
    //             'customer_gstin' => 123,
    //         ],
    //         [
    //             'customer_name' => 'Jay',
    //             'customer_contact_no' => 1234567890,
    //             'customer_address' => 'Indore',
    //             'customer_gstin' => 123,
    //         ]
    //     ];

    //     MstCustomer::insert($details);
    }


}
