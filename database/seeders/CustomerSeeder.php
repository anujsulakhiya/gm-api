<?php

namespace Database\Seeders;

use App\Models\MstCustomer;
use Illuminate\Database\Seeder;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MstCustomer::factory()->count(2)->create();
    }
}
