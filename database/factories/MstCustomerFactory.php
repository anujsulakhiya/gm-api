<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class MstCustomerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $customer_name = ['Omshri Chouhan', 'Aayush Majuriya', 'Anuj Sulakhiya', 'Harshit Chouhan'];

        return [
            'customer_name' => $customer_name[rand(0,3)],
            'customer_contact_no' => $this->faker->numberBetween(1000000000,9999999999),
            'customer_address' => $this->faker->address,
            'customer_gstin' => $this->faker->ssn,
        ];
    }
}
