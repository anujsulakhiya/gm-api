<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class MstBankFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'bank_name' => $this->faker->creditCardType,
            'account_no' => $this->faker->bankAccountNumber,
            'ifsc_code' => $this->faker->swiftBicNumber,
            'branch' => $this->faker->city,
        ];
    }
}
