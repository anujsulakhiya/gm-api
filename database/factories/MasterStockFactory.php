<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class MasterStockFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $non_inventory_stock_name = [
            'Garden For Marriage',
            'Garden For Birthday',
            'Garden For Meeting',
            'Garden For Party',
            'Garden For Seminar',
        ];

        $inventory_stock_name = [
            'Table',
            'Mate',
            'Chair',
            'Tent',
            'stage'
        ];


        $is_inventory = rand(0,1);
        $stock_qty = rand(1,1000);

        $stock_name = $inventory_stock_name[rand(0,4)];
        if($is_inventory == 0){
            $stock_qty = 1;
            $stock_name = $non_inventory_stock_name[rand(0,4)];
        }
        
    
        return [
            'firm_id' => rand(1,10),
            'stock_name' => $stock_name .' '. rand(1,1000),
            'stock_description' => $this->faker->text,
            'stock_mrp' => rand(1,1000),
            'stock_qty' => $stock_qty,
            'stock_available' => $stock_qty,
            'stock_gst' => 0,
            'stock_type' => 0,
            'is_inventory' => $is_inventory,
            'stock_value' => rand(100,1000),
        ];
    }
}
