<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class MasterFirmFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'firm_name' => $this->faker->company,
            'firm_contact_no' => $this->faker->numberBetween($min = 100000000, $max = 9999999999),
            'firm_address' => $this->faker->address,
            'firm_gstin' => $this->faker->ssn,
        ];
    }
}
