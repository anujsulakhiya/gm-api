<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->id();
            $table->integer('type');
            $table->integer('is_inventory');
            // $table->integer('invoice_id');
            // $table->foreignId('invoice_id')->constrained('customer_invoices')->onDelete('cascade');
            $table->foreignId('invoice_id') ->constrained('customer_invoices') ->onUpdate('cascade') ->onDelete('cascade');
            $table->string('service_id');
            // $table->integer('firm_id');
            $table->foreignId('firm_id')->constrained('master_firms');
            $table->integer('firm_invoice_count')->nullable();
            $table->double('rate');
            $table->double('extra_rate')->default(0);
            $table->double('qty_in')->default(0);
            $table->double('qty_out')->default(0);
            $table->double('qty_lost')->default(0);
            $table->double('lost_value')->default(0);
            $table->date('date');
            $table->integer('days');
            $table->string('time')->nullable();
            $table->integer('total');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
