<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->integer('account_id');
            $table->integer('voucher_id')->nullable();
            $table->string('voucher_head')->nullable();
            $table->date('tdate');
            $table->foreignId('customer_id')->constrained('mst_customers')->nullable();
            $table->foreignId('invoice_id')->constrained('customer_invoices')->nullable();
            // $table->integer('mst_expense_id')->constrained('customer_invoices');
            $table->integer('expense_id')->nullable();
            $table->integer('employee_id')->nullable();

            $table->double('debit')->default(0);
            $table->double('payment_in')->default(0);
            $table->double('payment_out')->default(0);
            $table->double('discount')->default(0);
            $table->string('remark')->nullable();
            $table->integer('payment_id');
            $table->integer('bank_id')->nullable();
            $table->integer('cheque_no')->nullable();
            $table->date('cheque_date')->nullable();
            $table->integer('cheque_status')->default(0);
            $table->date('cheque_clear_date')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
