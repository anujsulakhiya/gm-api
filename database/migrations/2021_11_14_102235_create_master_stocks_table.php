<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_stocks', function (Blueprint $table) {
            $table->id();
            $table->foreignId('firm_id')->constrained('master_firms');
            $table->string('stock_name');
            $table->string('stock_description')->nullable();
            $table->double('stock_mrp');
            $table->double('stock_qty')->default(1)->nullable();
            $table->double('stock_available')->default(0)->nullable();
            $table->double('stock_gst')->nullable();
            $table->double('stock_value')->nullable();
            $table->boolean('stock_type');
            $table->boolean('is_inventory');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_stocks');

    }
}
