<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerLedgersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_ledgers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('customer_id')->constrained('mst_customers');
            $table->foreignId('invoice_id')->constrained('customer_invoices');
            $table->integer('payment_type');
            $table->double('bill_amount')->default(0);
            $table->double('paid_amount')->default(0);
            $table->double('discount')->default(0);
            $table->double('amount');
            $table->string('remark')->nullable();
            $table->integer('payment_mode')->nullable();
            $table->integer('bank_id');
            $table->date('tdate');
            $table->double('balance');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_ledgers');
    }
}
