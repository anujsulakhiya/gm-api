<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_invoices', function (Blueprint $table) {
            $table->id();
            $table->string('invoice_number');
            // $table->string('invoice_customer_id');
            $table->foreignId('invoice_customer_id')->constrained('mst_customers');
            $table->date('invoice_date');
            $table->double('invoice_total')->default(0);
            $table->integer('total_remaining_qty')->default(0);
            $table->double('paid_amount')->default(0);
            $table->double('discount')->default(0);
            $table->double('debit')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_invoices');
    }
}
