<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAutocompletesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autocompletes', function (Blueprint $table) {
            $table->id();
            $table->string('master_keyword');
            $table->string('table_name');
            $table->string('return_column_name');
            $table->string('filter_column');
            $table->string('search_keyword');
            $table->string('limit');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autocompletes');
    }
}
